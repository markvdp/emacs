;;; emacs-configuration --- Emacs configuration

;;; Commentary:

;;; Code:

;;Record startup timestamp
(defvar super-emacs/invokation-time
  (current-time))

(setq config-basedir
      (file-name-directory
       (or (buffer-file-name) load-file-name)))

(defvar my-org-filesdir "~/Documents/Org-mode")

;;Load configuration files
(load-file "~/.emacs.d/cfg/system.el")
(load-file "~/.emacs.d/cfg/basic.el")
(load-file "~/.emacs.d/cfg/repositories.el")
(load-file "~/.emacs.d/cfg/navigation.el")
(load-file "~/.emacs.d/cfg/defaultpkgs.el")
(load-file "~/.emacs.d/cfg/flycheck.el")
(load-file "~/.emacs.d/cfg/projectile.el")
(load-file "~/.emacs.d/cfg/shell.el")
(load-file "~/.emacs.d/cfg/completion.el")
(load-file "~/.emacs.d/cfg/plantuml.el")
(load-file "~/.emacs.d/cfg/git.el")
(load-file "~/.emacs.d/cfg/flyspell.el")
(load-file "~/.emacs.d/cfg/templates.el")
(load-file "~/.emacs.d/cfg/python.el")
(load-file "~/.emacs.d/cfg/rust.el")
(load-file "~/.emacs.d/cfg/lsp.el")
;; (load-file "~/.emacs.d/cfg/dap.el")
(load-file "~/.emacs.d/cfg/org.el")
(load-file "~/.emacs.d/cfg/hydra.el")
(load-file "~/.emacs.d/cfg/adoc.el")
(load-file "~/.emacs.d/cfg/latex.el")

;;Print welcome message
(princ (concat "Welcome, Today is "
               (format-time-string "%B %d %Y")
               "  (Started in "
               (number-to-string (cadr (time-subtract (current-time)
                                                      super-emacs/invokation-time)))
               " seconds)")
       (get-buffer-create (current-buffer)))

(provide 'emacs-cfg)
;;; init.el ends here
