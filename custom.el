(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(yatemplate yasnippet-snippets yaml-mode winnow use-package-chords undo-tree smartparens rustic rainbow-delimiters quelpa-use-package python-black plantuml-mode paradox ox-reveal ox-pandoc ox-jira ox-gfm ox-asciidoc org-super-agenda org-projectile org-plus-contrib org-notebook org-dashboard org-chef ob-ipython ob-async monokai-theme magit-tbdiff lsp-ui ivy-yasnippet ivy-bibtex gitpatch git-timemachine git-msg-prefix flyspell-correct-ivy flycheck fic-mode expand-region eshell-fringe-status eldoc-overlay ebib diff-hl counsel company-quickhelp adoc-mode ace-window)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
