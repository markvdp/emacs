;;; package --- Generic setup
;;; Commentary:

;;; Code:
(use-package monokai-theme
  :ensure t
  :config
  (load-theme 'monokai t))

;; Rainbow stuff
(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
  )

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (progn
    (require 'smartparens-config)
    (smartparens-global-mode 1)
    (show-paren-mode t)))

(use-package fic-mode
  :commands fic-mode
  :ensure t
  :init (add-hook 'prog-mode-hook 'fic-mode)
  :config

  (defun fic-view-listing ()
    "Use occur to list related FIXME keywords"
    (interactive)
    (occur "\\<\\(FIXME\\|WRITEME\\|WRITEME!\\|TODO\\|BUG\\):?"))
  )

(use-package yaml-mode
  :ensure t
  :mode "\\.sls\\'"
  )

(use-package undo-tree
  :ensure t
  :config
  (global-undo-tree-mode)
)

(use-package diff-hl
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
  (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode))

(provide 'defaultpkgs-cfg)
;;; defaultpkgs.el ends here
