;;; basic -- basic configuration

;;; Commentary:

;;; Code:
;; Disable welcome screen
(setq inhibit-startup-message t)

;; Highlight matching parentheses when the point is on them.
(show-paren-mode t)

;; yes or no becomes y or n
(defalias 'yes-or-no-p 'y-or-n-p)

;; Enable line numbers globally
(global-linum-mode t)

;; use spaces, not tabs
(setq-default indent-tabs-mode nil)

;; disable line wrap
;;(setq default-truncate-lines t)

;; make side by side buffers function the same as the main window
(setq truncate-partial-width-windows nil)
;; Add F12 to toggle line wrap
(global-set-key (kbd "<f12>") (lambda() (interactive) (toggle-truncate-lines) (toggle-word-wrap)))

;; disable sound
(setq visible-bell 1)

;; Sync buffer with file
(global-auto-revert-mode t)

;; Shows column number
(column-number-mode 1)

;; Show file full path in title bar
(setq-default frame-title-format
	      (list '((buffer-file-name " %f"
					(dired-directory
					 dired-directory
					 (revert-buffer-function " %b"
								 ("%b - Dir:  " default-directory)))))))

(provide 'basic-cfg)
;;; basic.el ends here
