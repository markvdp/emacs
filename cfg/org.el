;;; Org -- Org configuration
;;; Commentary:

;;; Code:

(global-set-key (kbd "C-c a") 'org-agenda)

(require 'org)

(setq org-startup-indented t
      org-enforce-todo-dependencies t
      org-cycle-separator-lines 2
      org-blank-before-new-entry '((heading) (plain-list-item . auto))
      org-insert-heading-respect-content nil
      org-reverse-note-order nil
      org-deadline-warning-days 30
      org-table-export-default-format "orgtbl-to-csv"
      org-src-window-setup 'other-window
      org-clone-delete-id t
      org-cycle-include-plain-lists t
      org-src-fontify-natively t
      org-hide-emphasis-markers t)

(setq intemo-base "~/Documents/Org-mode/intemo")

;; Todo part
(setq org-agenda-files '())
(when (file-exists-p (concat intemo-base "/todo"))
  (setq org-agenda-files
        (append org-agenda-files (directory-files (concat intemo-base "/todo") t "^.*\\.org$"))))
(when (file-exists-p (concat intemo-base "/projects"))
  (setq org-agenda-files
        (append org-agenda-files (directory-files (concat intemo-base "/projects") t "^.*\\.org$"))))
(when (file-exists-p (concat intemo-base "/meetings"))
  (setq org-agenda-files
        (append org-agenda-files (directory-files (concat intemo-base "/meetings") t "^.*\\.org$"))))
(when (file-exists-p (concat intemo-base "/management"))
  (setq org-agenda-files
        (append org-agenda-files (directory-files (concat intemo-base "/management") t "^.*\\.org$"))))
(when (file-exists-p (concat intemo-base "/stage"))
  (setq org-agenda-files
        (append org-agenda-files (directory-files (concat intemo-base "/stage") t "^.*\\.org$"))))

;; Some agenda commands
(setq org-agenda-custom-commands '(
                                   ("D" todo "DONE")
                                   ("w" "Work and administrative"
                                    ((agenda)
                                     (tags-todo "WORK")
                                     (tags-todo "OFFICE")
                                     (tags-todo "ADMIN")))
                                   ("p" "personnal"
                                    ((agenda)
                                     (tags-todo "PERSONNAL")))
                                   ("d" "Daily Action List"
                                    ((agenda "" ((org-agenda-ndays 1)
                                                 (org-agenda-sorting-strategy
                                                  '((agenda time-up priority-down tag-up) ))
                                                 (org-deadline-warning-days 0)))))))

(setq org-plantuml-jar-path (expand-file-name "~/.emacs.d/plantuml/plantuml.jar"))
(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

;; (use-package ox-odt
;;   :ensure t
;;   :config
;;   (custom-set-variables
;;    '(org-odt-convert-process "LibreOffice")
;;    '(org-odt-preferred-output-format "docx")
;;    '(org-odt-transform-processes
;;      '(("Optimize Column Width of all Tables"
;;         "soffice" "--norestore" "--invisible" "--headless"
;;         "macro:///OrgMode.Utilities.OptimizeColumnWidth(%I)")
;;        ("Update All"
;;         "soffice" "--norestore" "--invisible" "--headless"
;;         "macro:///OrgMode.Utilities.UpdateAll(%I)")
;;        ("Reload"
;;         "soffice" "--norestore" "--invisible" "--headless"
;;         "macro:///OrgMode.Utilities.Reload(%I)")
;;        ))
;;    ;; '(org-latex-to-mathml-convert-command "java -jar %j -unicode -force -df %o %I")
;;    ;; '(org-latex-to-mathml-jar-file "/home/kjambunathan/Downloads/mathtoweb.jar")
;;    )

;;   (require 'ox-odt)

;;   ;; (setcdr (assq 'system org-file-apps-defaults-gnu) "xdg-open %s")
;;   (setcdr (assq 'system org-file-apps-gnu) "xdg-open %s")
;;   (advice-add 'org-open-file :around
;;               (lambda (orig-fun &rest args)
;;                 ;; Work around a weird problem with xdg-open.
;;                 (let ((process-connection-type nil))
;;                   (apply orig-fun args))))
;;   )

(use-package org-projectile
  :ensure t
  :after org
  :after projectile
  :config
  (org-projectile-per-project)
  (setq org-projectile-per-project-filepath "todo.org"
        org-agenda-files
        (append org-agenda-files (org-projectile-todo-files)))
  )

;; On Error: json-readtable-error 47
;; for this to work: apt install jupyter, or, pip install -U --user jupyter.
(use-package org-capture
  :config
  (global-set-key (kbd "C-c c") 'org-capture)

  ;; Capture
  (setq org-capture-templates
        `(("b" "Adding book" entry
           (file+headline ,(format "%s/intemo/todo/todo.org" my-org-filesdir) "To read")
           ;; (file ,(format "%s/third_parties/org-capture-templates/book.org" config-basedir))
           )
          
          ("L" "Bookmark" entry
           (file+olp ,(format "%s/intemo/todo/todo.org" my-org-filesdir) "To review" "Bookmarks")
           ;; (file ,(format "%s/third_parties/org-capture-templates/bookmark.org" config-basedir))
           )

          ("m" "mail" entry
           (file+headline ,(format "%s/intemo/todo/todo.org" my-org-filesdir) "Mailing")
           ;; (file ,(format "%s/third_parties/org-capture-templates/mail.org" config-basedir))
           )

          ("M" "MSP calendar" entry
           (file ,(format "%s/intemo//Calendar-MSP.org" my-org-filesdir))
           ;; (file ,(format "%s/third_parties/org-capture-templates/calendar.org" config-basedir))
           )

          ("P" "Personnal calendar" entry
           (file ,(format "%s/intemo//Calendar-Personal.org" my-org-filesdir))
           ;; (file ,(format "%s/third_parties/org-capture-templates/calendar.org" config-basedir))
           )

          ("t" "ToDo Entry" entry
           (file+headline ,(format "%s/intemo/todo/todo.org" my-org-filesdir) "To sort")
           ;; (file ,(format "%s/third_parties/org-capture-templates/default.org" config-basedir))
           )
          )
        )
  )

; Define specific modes for specific tools
(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
(add-to-list 'org-src-lang-modes '("dot" . graphviz-dot))


(use-package ox-html
  :after ox
  :requires (htmlize)
  :config
  (setq org-html-xml-declaration '(("html" . "")
                                 ("was-html" . "<?xml version=\"1.0\" encoding=\"%s\"?>")
                                 ("php" . "<?php echo \"<?xml version=\\\"1.0\\\" encoding=\\\"%s\\\" ?>\"; ?>"))
      org-export-html-inline-images t
      org-export-with-sub-superscripts nil
      org-export-html-style-extra "<link rel=\"stylesheet\" href=\"org.css\" type=\"text/css\" />"
      org-export-html-style-include-default nil
      org-export-htmlize-output-type 'css ; Do not generate internal css formatting for HTML exports
      )

  (defun endless/export-audio-link (path desc format)
    "Export org audio links to hmtl."
    (cl-case format
      (html (format "<audio src=\"%s\" controls>%s</audio>" path (or desc "")))))
  (org-add-link-type "audio" #'ignore #'endless/export-audio-link)


  (defun endless/export-video-link (path desc format)
    "Export org video links to hmtl."
    (cl-case format
      (html (format "<video controls src=\"%s\"></video>" path (or desc "")))))
  (org-add-link-type "video" #'ignore #'endless/export-video-link)

  (add-to-list 'org-file-apps '("\\.x?html?\\'" . "/usr/bin/vivaldi-stable %s"))
  )

(use-package ox-reveal
  :ensure t
  :requires (ox-html htmlize)
  )

(use-package ox-latex
  :after ox
  :defer t
  :config
  (setq org-latex-listings t
        org-export-with-LaTeX-fragments t
        org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))
  )

;; Asciidoc export
(use-package ox-asciidoc
  :ensure ;t
  :after ox
  )

(provide 'org-cfg)
;;; org.el ends here
