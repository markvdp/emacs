;;; python --- python setup

;;; Commentary:

;;; Code:

(use-package python-black
  :ensure t)


;; (use-package  lsp-pyre
;;   :requires lsp-mode
;;   :ensure t)

(provide 'python-cfg)
;;; python.el ends here
