;;; packages -- setup package initialisation

;;; Commentary:

;;; Code:
(require 'package)

;; first, declare repositories
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
;;        ("marmalade" . "http://marmalade-repo.org/packages/")
        ("melpa" . "http://melpa.org/packages/")
        ("ox-odt" . "https://kjambunathan.github.io/elpa/")
        )
      )

;; (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

;;Initialize package.el
(package-initialize)

;;Create repositories cache, if required
(when (not package-archive-contents)
  (package-refresh-contents))

;; List of packages
(defvar package-list)
(setq package-list '(use-package))

;; Install all packages from package-list that are not
;; already installed
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; Enhanced package management
(use-package paradox
  :ensure t
  :defer t
  :config
  (setq paradox-spinner-type 'progress-bar
        paradox-execute-asynchronously t)
  )

(use-package quelpa-use-package
  :ensure t
  :init
  (setq quelpa-update-melpa-p nil)
  )

(use-package use-package-chords
  :ensure t
  :config (key-chord-mode 1)
  )

;; el-get
;; Not using it at the moment.
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(el-get 'sync)


(provide 'packages-cfg)
;;; repositories.el ends here
