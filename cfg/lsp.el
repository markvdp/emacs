;;; package --- LSP setup
;;; Commentary:

;;; Code:

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-show-code-actions t)
  (lsp-ui-doc-enable t)
)

(use-package lsp-mode
  ;; :requires hydra
  :ensure t
  :hook ((c++-mode . lsp)
         (c-mode . lsp)
         (python-mode . lsp)
         (rustic-mode . lsp)
         )
  :custom
  (rustic-lsp-server 'rust-analyzer)
  (lsp-rust-analyzer-server-command '("~/.cargo/bin/rust-analyzer"))
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.5)
  :custom
  ;; Setup python language server settings
  (lsp-pyls-plugins-flake8-enabled t)
  ;; (lsp-register-custom-settings
  ;;  '(("pyls.plugins.pyls_mypy.enabled" t t)
  ;;    ("pyls.plugins.pyls_mypy.live_mode" nil t)
  ;;    ("pyls.plugins.pyls_black.enabled" t t)
  ;;    ("pyls.plugins.pyls_isort.enabled" t t)

  ;;    ;; Disable these as they're duplicated by flake8
  ;;    ("pyls.plugins.pycodestyle.enabled" nil t)
  ;;    ("pyls.plugins.mccabe.enabled" nil t)
  ;;    ("pyls.plugins.pyflakes.enabled" nil t)))
  )

(provide 'lsp-cfg)
;;; lsp.el ends here
