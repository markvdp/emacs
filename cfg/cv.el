(use-package ox-moderncv
  :load-path "/home/mark/dev/src/org-cv/"
  :init (require 'ox-moderncv)
  :config
  (defun org-cv-moderncv-export ()
    "Export cv.org to pdf"
    (interactive)
    (let ((filename (concat temporary-file-directory (file-name-base) ".tex")))
      (org-export-to-file 'moderncv filename)
      (org-latex-compile filename)))
  )
