;;; navigation --- Navigation tools

;;; Commentary:

;;; Code:

;; With Avy, you can move point to any position in Emacs – even in a
;; different window – using very few keystrokes. For this, you look at
;; the position where you want point to be, invoke Avy, and then enter
;; the sequence of characters displayed at that position.
(use-package avy
  :ensure t
  :bind (("M-s" . avy-goto-word-1)))

(use-package ace-window
  :ensure t
  :bind (("M-o" . ace-window))
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))

(provide 'navigation-cfg)
;;; navigation.el ends here
