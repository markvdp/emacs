;;; Templates --- template configuration
;;; Commentary:
;;; Code:
(use-package yasnippet
  :ensure t
  :config

  ;; Adding yasnippet support to company
  (add-to-list 'company-backends '(company-yasnippet))

  ;; Activate global
  (yas-global-mode))

(use-package yasnippet-snippets
  :ensure t
  )

(use-package ivy-yasnippet
  :ensure t)

;; yatemplate (an over-layer of yasnippet) coupled with auto-insert to
;; have a set of file type dedicated templates. The templates are
;; available in third_parties/templates directory.
(use-package yatemplate
  :ensure t
  :after yasnippet
  :config

  ;; Define template directory
  (setq yatemplate-dir (concat config-basedir "/third_parties/templates"))

  ;; Coupling with auto-insert
  (setq auto-insert-alist nil)
  (yatemplate-fill-alist)
  ;; (add-hook 'find-file-hook 'auto-insert)
  )
(provide 'template-cfg)
;;; templates.el ends here
