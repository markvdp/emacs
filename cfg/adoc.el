;;; adoc -- configure adoc

;;; Commentary:

;;; Code:
(use-package adoc-mode
  :ensure t
  )

(provide 'adoc-cfg)
;;; adoc.el ends here
