;;; git --- git setup
;;; Commentary:

;;; Code:

;; Quickly page through the history of a file with git-timemachine
(use-package git-timemachine
  :ensure t)

(use-package magit
  :ensure t
  :bind (("C-x g" . magit))
  :config

  ;; Ignore recent commit
  (setq magit-status-sections-hook
        '(magit-insert-status-headers
          magit-insert-merge-log
          magit-insert-rebase-sequence
          magit-insert-am-sequence
          magit-insert-sequencer-sequence
          magit-insert-bisect-output
          magit-insert-bisect-rest
          magit-insert-bisect-log
          magit-insert-untracked-files
          magit-insert-unstaged-changes
          magit-insert-staged-changes
          magit-insert-stashes
          magit-insert-unpulled-from-upstream
          magit-insert-unpulled-from-pushremote
          magit-insert-unpushed-to-upstream
          magit-insert-unpushed-to-pushremote))
  )

(use-package magit-tbdiff
  :ensure t
  :after magit
  )

;; FIXME: first infocation of magit reports "Please add some keywords"
(use-package magit-todos
  :disabled t
  :ensure t
  :commands (magit-todos-mode)
  :hook (magit-mode . magit-todos-mode)
  :config
  (setq magit-todos-recursive t
        magit-todos-depth 100)
  :custom (magit-todos-keywords (list "TODO" "FIXME"))
  )

(provide 'git-cfg)
;;; git.el ends here
