;;; flyspell -- configure flyspell

;;; Commentary:

;;; Code:

(use-package flyspell
  :ensure t
  :hook ((prog-mode . flyspell-prog-mode)
         (text-mode . flyspell-mode)
         (org-mode  . flyspell-mode))
  :config
  ;; Hunspell setup
  (setq ispell-program-name "hunspell")
  (setq ispell-local-dictionary "myhunspell") ; "myhunspell" is key to lookup in `ispell-local-dictionary-alist`
  (setq ispell-dictionary "en_US")

  ;; ;; aspell setup
  ;; (setq ispell-program-name "aspell")
  ;; ;; You could add extra option "--camel-case" for since Aspell 0.60.8
  ;; ;; @see https://github.com/redguardtoo/emacs.d/issues/796
  ;; (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US" "--run-together" "--run-together-limit=16"))
  
  (bind-key "C-c N"
            (lambda ()
              (interactive)
              (ispell-change-dictionary "nl_NL")
              (flyspell-buffer))
            )
  
  (bind-key "C-c E"
            (lambda ()
              (interactive)
              (ispell-change-dictionary "en_US")
              (flyspell-buffer))
            )
  )

(use-package flyspell-correct-ivy
  :ensure t
  :demand t
  :bind (:map flyspell-mode-map
              ("C-;" . flyspell-correct-wrapper))
  )

(provide 'flyspell-cfg)
;;; flyspell.el ends here
