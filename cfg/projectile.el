;;; package -- Projectile setup
;;; Commentary:

;;; Code:
(use-package projectile
  :ensure t
  :bind-keymap (("C-c p" . projectile-command-map))
  :config
  ;; Global configuration
  (setq projectile-switch-project-action 'neotree-projectile-action
        projectile-enable-caching t
        projectile-create-missing-test-files t
        projectile-switch-project-action #'projectile-commander
        projectile-ignored-project-function 'file-remote-p
        projectile-completion-system 'ivy)

  ;; ;; Defining some helpers
  ;; (def-projectile-commander-method ?s
  ;;   "Open a *shell* buffer for the project."
  ;;   ;; This requires a snapshot version of Projectile.
  ;;   (projectile-run-shell))

  ;; (def-projectile-commander-method ?c
  ;;   "Run `compile' in the project."
  ;;   (projectile-compile-project nil))

  ;; (def-projectile-commander-method ?\C-?
  ;;   "Go back to project selection."
  ;;   (projectile-switch-project))

  ;; Keys
  ;;(setq projectile-keymap-prefix (kbd "C-x p"))

  ;; Activate globally
  (projectile-mode t)
  )

(provide 'projectile-cfg)
;;; projectile.el ends here
