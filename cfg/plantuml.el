;;; plantuml -- plantuml configuration

;;; Commentary:
;;; Code:

(use-package plantuml-mode
  :ensure t
  :ensure t
  :custom
  
  (plantuml-jar-path "~/.emacs.d/plantuml/plantuml.jar")
  (plantuml-default-exec-mode 'jar)
  (org-plantuml-jar-path (expand-file-name "~/.emacs.d/plantuml/plantuml.jar"))
  (org-startup-with-inline-images t)
  ;; (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
  ;; (org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))
  )

(provide 'plantuml-cfg)
;;; plantuml.el ends here
