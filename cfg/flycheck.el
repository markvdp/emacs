;;; flycheck --- Flycheck setup
;;; Commentary:


;; Install language checkers
;; eg. for ubuntu:
;;     epython3 - apt install pylint3 python3-pylint-django python3-pytest-pylint

;;; Code:

(use-package let-alist)
(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  )

(provide 'flycheck-cfg)
;;; flycheck.el ends here
