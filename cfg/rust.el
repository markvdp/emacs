;;; rust --- rust setup
;;; Commentary:
;;; Code:

;; $ git clone https://github.com/rust-analyzer/rust-analyzer.git
;; $ cd rust-analyzer; $ cargo xtask install --server
;;   # will install rust-analyzer into $HOME/.cargo/bin
;; Copy rust-analyzer to path

(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :custom
  ;; (lsp-eldoc-hook nil)
  (rustic-format-on-save t)
  ;; uncomment for less flashiness
  ;; (lsp-eldoc-hook nil)
  ;; (lsp-enable-symbol-highlighting nil)
  ;; (lsp-signature-auto-activate nil)
  :config
  (dap-register-debug-template "Rust::GDB Run Configuration"
                               (list :type "gdb"
                                     :request "launch"
                                     :name "GDB::Run"
                             :gdbpath "/home/mark/.cargo/bin/rust-gdb"
                                     :target nil
                                     :cwd nil))
  
  )

(provide 'rust-config)
;;; rust.el ends here
