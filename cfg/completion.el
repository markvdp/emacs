;;; Completion --- completion stuff
;;; Commentary:

;;; Code:
(use-package ivy
  :ensure t
  :bind (("C-s" . swiper))
  :config
  (ivy-mode)
  (setq ivy-display-style 'fancy
        ivy-use-virtual-buffers t
        enable-recursive-minibuffers t
        ivy-use-selectable-prompt t))

(use-package ivy-bibtex
  :ensure t
  :config
  (setq bibtex-completion-bibliography
      '("~/Documents/reference.bib"
        ;;  "/path/to/bibtex-file-2.bib"
        )
      )
)

;; counsel is a collection of ivy enhanced versions of common Emacs commands.
(use-package counsel
  :ensure t
  :config
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-x C-f") 'counsel-find-file))


;; Global company
(use-package company
  :ensure t
  :config
  ;; Global
  (setq company-idle-delay 1
        company-minimum-prefix-length 1
        company-show-numbers t
        company-tooltip-limit 20)

  ;; Default backends
  (setq company-backends '((company-files)))

  ;; Activating globally
  (global-company-mode t))

;; ;TODO: colors are really funky
;; (use-package company-quickhelp
;;   :ensure t
;;   :after company
;;   :config
;;   (company-quickhelp-mode 1))

(use-package eldoc-overlay
  :ensure t
  :init (eldoc-overlay-mode 1)
  :config
  (setq eldoc-overlay-backend 'quick-peek))


(provide 'completion-config)
;;; completion.el ends here
