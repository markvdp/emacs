;;; package --- Configure custom latex
;;; Commentary:
;;; Code:

(use-package ox-latex
  :config
  ;; Make sure that the sty files are in ~/texmf/tex/latex/intemo
  (add-to-list 'org-latex-classes
                  '("book"
                    "\\documentclass[a4paper]{book}
                     [NO-DEFAULT-PACKAGES]
                     [PACKAGES]
                     [EXTRA]"
                    ("\\chapter{%s}" . "\\chapter*{%s}")
                    ("\\section{%s}" . "\\section*{%s}")
                    ("\\subsection{%s}" . "\\subsection*{%s}")
                    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))

  (setq org-latex-pdf-process
        '("lualatex -shell-escape -interaction nonstopmode %f"
          "lualatex -shell-escape -interaction nonstopmode %f"))

  (setq luamagick '(luamagick :programs ("lualatex" "magick")
                              :description "pdf > png"
                              :message "you need to install lualatex and imagemagick."
                              :use-xcolor t
                              :image-input-type "pdf"
                              :image-output-type "png"
                              :image-size-adjust (1.0 . 1.0)
                              :latex-compiler ("lualatex -interaction nonstopmode -output-directory %o %f")
                              :image-converter ("magick convert -density %D -trim -antialias %f -quality 100 %O")))
  (add-to-list 'org-preview-latex-process-alist luamagick)
  (setq org-preview-latex-default-process 'luamagick)
)

;; (use-package ebib
;;   :ensure t
;;   :config
;;   (org-add-link-type "ebib" 'ebib)
;;   (setq org-bibtex-file "~/Documents/references.bib")
;;   )


(provide 'latex-cfg)
;;; latex.el ends here
